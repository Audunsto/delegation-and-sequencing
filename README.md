# The delegation-and-sequencing problem

The delegation-and-sequencing problem is a core problem of emergency management. In brief outline it is the problem of allocating manpower to collaborative tasks under constraints on the proper ordering of these tasks. That is, one is presented with a set of tasks, a set of teams or companies of responders, and a set of constraints on the temporal ordering of these tasks. The problem is to allocate manpower in a way that respects the temporal constraints including *implicit ones* that may arise from the interplay between the collaborative structure of the tasks and the actual size of the teams to which these tasks can be delegated at a given moment.

# This repository

Contains a demo implementation of the above mentioned problem applied to an operation procedure belonging to the East Aurora Fire Department, US (cf. [EAFD](https://www.researchgate.net/publication/256017533_East_Aurora_Fire_Department)). It is meant to illustrate the nucleus of a machine reasoning system to be used in a *pre-incident review* of an emergency management agency to display the delegation and sequencing structure inherent in a plan. The purpose is to uncover weaknesses in the initial plan and to optimize sequencing and delegation to increase the likelihood of achieving goals. 

The computation of the action sequences is implemented in *multishot ASP* as an interactive process accepting revisions to the orginal plan. It thus also supports *during-incident reviews*. The idea is to adapt the plan, understood as the distribution of responsibilities over time, as the incident evolves and contingencies happen. Here the coordination issues concern rescheduling of tasks in response to events that make the initial deployment of an operating procedure unrealizable for some reason or other.

# Test it 

The code can be run from command line with a python script. It pressupposes that [Clingo](https://potassco.org/clingo/) has been installed. Running

`python3 controller.py`

produces an initial assignment of responsibilities according to the EAFD specifications. It is formatted ever so little by a pretty printer that distinguishes collaborative from singular actions, viz.:

```
#################################################
#      Optimal model of cost [1, 15]         
#################################################

Agent john, help expedite a at time 1
 Action description: "Department turn-out"

Agent barry, help expedite a at time 1
 Action description: "Department turn-out"

Agent frank, help expedite a at time 1
 Action description: "Department turn-out"

Agent jan, help expedite a at time 1
 Action description: "Department turn-out"

Agent liz, help expedite a at time 1
 Action description: "Department turn-out"

Agent kathrin, help expedite a at time 1
 Action description: "Department turn-out"

Agent john, help expedite b at time 2
 Action description: "Travel of attack engine to 911 address"

Agent barry, help expedite b at time 2
 Action description: "Travel of attack engine to 911 address"

Agent frank, help expedite b at time 2
 Action description: "Travel of attack engine to 911 address"

Agent jan, help expedite c at time 2
 Action description: "Travel of second engine to 911 address"

Agent liz, help expedite c at time 2
 Action description: "Travel of second engine to 911 address"

Agent kathrin, help expedite c at time 2
 Action description: "Travel of second engine to 911 address"

Agent john, help expedite e at time 3
 Action description: "Attack engine crew advance 1.75 inch hose to seat of fire"

Agent frank, help expedite e at time 3
 Action description: "Attack engine crew advance 1.75 inch hose to seat of fire"

Agent jan, help expedite i at time 3
 Action description: "Second engine drop 5 inch hose between attack engine and nearest hydrant"

Agent liz, help expedite h at time 3
 Action description: "Second engine crew member prepare nearest hydrant for hook-up"

Agent kathrin, help expedite i at time 3
 Action description: "Second engine drop 5 inch hose between attack engine and nearest hydrant"

Agent xi, help expedite d at time 3
 Action description: "Travel of ladder tower to 911 address"

Agent lin, help expedite d at time 3
 Action description: "Travel of ladder tower to 911 address"

Agent dahn, help expedite d at time 3
 Action description: "Travel of ladder tower to 911 address"

Agent barry, expedite action f at time 3
 Action description: "Attack engine driver prepare to pump water"

Agent john, help expedite g at time 4
 Action description: "Attack fire with tank water aboard attack engine"

Agent barry, help expedite g at time 4
 Action description: "Attack fire with tank water aboard attack engine"

Agent jan, help expedite j at time 4
 Action description: "Second engine hook to hydrant and to attack engine and prepare to pump water"

Agent liz, help expedite j at time 4
 Action description: "Second engine hook to hydrant and to attack engine and prepare to pump water"

Agent kathrin, help expedite l at time 4
 Action description: "Set up ladder tower"

Agent xi, help expedite l at time 4
 Action description: "Set up ladder tower"

Agent john, help expedite rem_win at time 5
 Action description: "remove windows"

Agent barry, help expedite rem_win at time 5
 Action description: "remove windows"

Agent jan, help expedite k at time 5
 Action description: "Attack fire with hydrant water"

Agent liz, help expedite k at time 5
 Action description: "Attack fire with hydrant water"

```

The cost represents the similarity of this plan to the previous one, as explained below. 


The program accepts inputs of form 

`add | remove (Agent, Action, Time)`

For instance

`add(e, john,3)`

Changes the intial plan to make John responsible for expediting task *e* at time 3, provided that that is possible. Similarly

`remove(e, john,3)`

Instructs the program to *relieve* John of that responsibility at time 3, and to redelegate task *e* to someone else.

If a particular revision of the original plan is inconsistent with the specification, then the original plan still stands. The program simply alerts the user and continues to wait for input.

If a new plan is produced it heeds *the maxim of minimal change*, meaning that it is as similar to the previous one as logically possible. The idea is the natural one of adaptation by least change. 


